from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from .utils import *
from .forms import IconUploadForm, LogoUploadForm
import re

name_prefix_unterkategorien = 'name_uk_'
name_prefix_prio = 'name_pr_'
name_suche = 'search'


def home(response):
    massnahmen_liste = list(Massnahme.objects.all().order_by('Maßnahme'))
    kategorien_liste = list(Kategorie.objects.all())
    priorisierungen_liste = list(Priorisierungen.objects.all())
    # Für jede Kategorie wird eine Liste mit den dazugehörigen Unterkategorien erstellt und als ein Element an die
    # große Unterkategorienliste angehängt.
    unterkategorien_liste = [list(UnterKategorie.objects.filter(ueber_kategorie=k)) for k in kategorien_liste]

    # !!! NORMALER REFRESH → Volle website mit allen Massnahmen wird returned
    if not 'MX-Request' in response.headers:
        uk_mit_icons = get_ok_mit_icons(massnahmen_liste, kategorien_liste)
        quellen = get_quellen_zu_massnahmen(massnahmen_liste)
        prios = get_prio_zu_massnahme(massnahmen_liste)
        slider = []
        massnahmen_liste = sort_massnahmen_nach_prio(slider, massnahmen_liste)
        infotexte = get_infotexte(
            ["extra-info-ueberschrift", "extra-info-massnahmen", "extra-info-priorisierung", "extra-info-kategorien"])

        return render(response, 'user_ui.html',
                      {"massnahmen_liste": zip(massnahmen_liste, uk_mit_icons, quellen, prios),
                       "kat_struct": zip(kategorien_liste, unterkategorien_liste),
                       "priorisierungs_liste": priorisierungen_liste,
                       "infotext_ueberschrift": infotexte["extra-info-ueberschrift"],
                       "infotext_massnahmen": infotexte["extra-info-massnahmen"],
                       "infotext_priorisierung": infotexte["extra-info-priorisierung"],
                       "infotext_kategorien": infotexte["extra-info-kategorien"]
                       })

    # !!! REFRESH WEGEN NUTZEREINGABE -> Nur Suchvorschläge und Maßnahmenliste werden returned

    # keys der gecheckten Checkboxes werden aus der Request geholt
    keys = list(response.GET.dict().keys())
    # Prefix wird entfernt dh. aus name_uk_Druckluft wird Druckluft
    checkboxen_namen = [e[len(name_prefix_unterkategorien):] for e in keys if
                        e.startswith(name_prefix_unterkategorien)]
    # Alle massnahmen, mit passenden Unterkategrorien werden geholt
    massnahmen_liste = get_massnahmen_zu_unterkategorien(checkboxen_namen)

    # Falls Suchbegriff vorhanden werden Massnahmen gefiltert (in Massnahmentitel oder Beschreibung)
    vorschlaege = None
    rechtschreiber = None
    suchbegriff = None
    if name_suche in keys:
        suchbegriff = response.GET.dict()[name_suche]
        suchbegriff = suchbegriff.lower()
        x = re.search("(\w([-]|\s)*)*", suchbegriff)
        suchbegriff = x.string[x.span()[0]:x.span()[1]]

        vorschlaege, rechtschreiber = generate_vorschlaege(suchbegriff, massnahmen_liste)
        # Suchbegriff und massnahmenliste werden 'Normalisiert' & gefiltert um treffer zu erleichtern
        massnahmen_liste = [m for m in massnahmen_liste if
                            suchbegriff in re.sub("[^A-Za-zöüäß -]", "", m.Maßnahme).lower() or
                            suchbegriff in re.sub("[^A-Za-zöüäß -]", "", m.Beschreibung).lower()
                            ]

    # Sortierung der Massnahmen nach Suchbegriff
    slider = [(key[8:], value) for key, value in response.GET.dict().items() if
              key.startswith(name_prefix_prio)]
    massnahmen_liste = sort_massnahmen_nach_prio(slider, massnahmen_liste)

    # Response bei keinen Gefundenen Massnahmen
    if not massnahmen_liste:
        if rechtschreiber and suchbegriff:
            return render(response, "partials/massnahmen_liste_rechtschreibfehler.html",
                          {"suchbegriff": suchbegriff, "rechtschreibfehler": rechtschreiber})
        return HttpResponse(b'<h1> Keine Massnahmen gefunden :)</h1>')

    # Response mit ausschließlich selektierten Massnahmen wird returned
    uk_mit_icons = get_ok_mit_icons(massnahmen_liste, kategorien_liste)
    quellen = get_quellen_zu_massnahmen(massnahmen_liste)
    prios = get_prio_zu_massnahme(massnahmen_liste)
    context = {"massnahmen_liste": zip(massnahmen_liste, uk_mit_icons, quellen, prios), "suchvorschlaege": vorschlaege}

    return render(response, 'partials/suchvorschlaege.html', context)


# Eröffne File Systeme für die Importe der Logos und Icons
Speicher = FileSystemStorage()
LogoSpeicher = FileSystemStorage(location='main/static/media')


def icon_upload(request):
    template = loader.get_template('admin/icon_upload.html')
    if request.method == 'POST':
        form = IconUploadForm(request.POST, request.FILES)
        if form.is_valid():
            # Variablen Für Rückmeldung initialisieren
            count = 0
            skipped = []
            # Für jede Ausgesuchte Datei spiechern, falls es eine .svg ist und name mit einer Unterkategorie übereinstimmt.
            for icon in request.FILES.getlist('icons'):
                if icon.name.split('.')[1] == 'svg':
                    if (UnterKategorie.objects.filter(icon__iendswith=icon.name).exists()):
                        if (Speicher.exists('icons/%s' % icon.name)):
                            Speicher.delete('icons/%s' % icon.name)
                        Speicher.save('icons/%s' % icon.name, icon.file)
                        count += 1
                    else:
                        skipped.append(icon.name)
                else:
                    skipped.append(icon.name)
            messages.success(request, '%s Icons wurden gespeichert' % count)
            messages.error(request, 'Nicht gespeichert wurden:  ' + ' | '.join(skipped))
        return HttpResponseRedirect('/admin/main/unterkategorie/icon_upload')
    else:
        form = IconUploadForm
    return render(request, 'admin/icon_upload.html', {'form': form})


def logo_upload(request):
    template = loader.get_template('admin/logo_upload.html')
    if request.method == 'POST':
        form = LogoUploadForm(request.POST, request.FILES)
        if form.is_valid():
            # Falls Datei eine .svg ist als Logo abspeichern.
            logo = request.FILES.get('logo')
            if logo.name.split('.')[1] == 'svg':
                if (LogoSpeicher.exists('PTW-logo.svg')):
                    LogoSpeicher.delete('PTW-logo.svg')
                LogoSpeicher.save('PTW-logo.svg', logo.file)
                messages.success(request, 'Logo wurde gespeichert.')
            else:
                messages.error(request, 'Logo muss eine .svg Datei sein. Logo wurde nicht gespeichert.')
        return HttpResponseRedirect('/admin/main/unterkategorie/logo_upload')
    else:
        form = LogoUploadForm
    return render(request, 'admin/logo_upload.html', {'form': form})
