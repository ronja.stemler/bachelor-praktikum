/*
    param: slider_value: nummerischer Wert des sliders als String
    return: Beschreibung des slider wertes als Wort
 */
function prio_val_2_text(slider_value) {
    slider_value = parseInt(slider_value);
    let prefix = "Relevanz : ";
    switch (slider_value) {
        case 1:
            return prefix + "niedrig";
        case 2:
            return prefix + "mittel";
        case 3:
            return prefix + "hoch";
        default:
            return "unzulässig";
    }
}

/*
    Dieser Code fügt allen Priorisierungen eine checkbox zur an/ab-wahl hinzu.
 */
const prio_list = document.getElementsByClassName("priorisierungen");
for (let i = 0; i < prio_list.length; i++) {
    // für jede Prio wird die checkbox, den slider und das label geholt
    let checkbox = prio_list[i].querySelector('input[type="checkbox"]');
    let slider = prio_list[i].querySelector('input[type="range"]');
    let label = slider.nextElementSibling;

    // Action listner wird der checkbox hinzugefügt, welcher auf 'click' reagiert
    checkbox.addEventListener("click", function () {
        // css klasse für Prio wird gesetzt/deaktiviert
        prio_list[i].classList.toggle("priorisierungen-inaktiv");
        // Slider wird aktiviert/deaktiviert und der label Text wird entsprechend gesetzt
        if (slider.hasAttribute("disabled")) {
            label.innerHTML = prio_val_2_text(slider.value);
            slider.removeAttribute("disabled");
        } else {
            label.innerHTML = "Relevanz : irrelevant";
            slider.setAttribute("disabled", "");
        }
    });
}

/*
    Dieser code implementiert die funktion des Auskalppens des Prio-Ordners, dazu bekommt der das div id-priorisierungen-kriterien
    einen Eventlistner wwelcher auf click reagiert.
*/
const prio_button = document.getElementById("id-priorisierungen-kriterien");
const prio_folder = document.getElementById("id-priorisierungen-auswahl");
const prio_info = document.getElementById("info-button-priorisierung")
prio_button.addEventListener("click", function (e) {
    // Clicks auf den Info-Knopf werden ignoriert
    if (prio_folder.contains(e.target) || prio_info.contains(e.target))
        return;
    // Wenn Ordner visible dann wird er invisible gesetzt und umgekehrt
    if (prio_folder.style.visibility === "hidden") {
        prio_folder.style.visibility = "visible";
        prio_button.classList.add("priorisierungs-kriterien-open");
    } else {
        prio_button.classList.remove("priorisierungs-kriterien-open");
        prio_folder.style.visibility = "hidden";
    }

})
// Wenn ausherhalb des Prio-Ordners gecklickt wird schließt sich dieser
window.addEventListener("click", function (e) {
    if (!prio_button.contains(e.target)) {
        prio_button.classList.remove("priorisierungs-kriterien-open");
        prio_folder.style.visibility = "hidden";
    }
})
