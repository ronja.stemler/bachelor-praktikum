/*
    Dieser Code fügt an jeden Oberkategorien-Knopf einen Event-listener hinzu,
    welcher bei dem Anklicken die ihm folgende unterkategorien-liste aus/einfährt.
 */
const ok_button_list = document.getElementsByClassName("oberkategorie-knopf");
for (let i = 0; i < ok_button_list.length; i++) {
    ok_button_list[i].addEventListener("click", function (e) {

        if (e.target.className === "oberkategorie-check-alle") return;

        this.classList.toggle("oberkategorie-aktiv");
        /* sibling von der Oberkategorie-knopf ist die Unterkategorie list, da sie im html Dokument als Nächstes kommt*/
        let related_uk_list = this.nextElementSibling;
        if (related_uk_list.style.maxHeight) {
            related_uk_list.style.maxHeight = null;
        } else {
            related_uk_list.style.maxHeight = related_uk_list.scrollHeight + "px";
        }
    });
}

/*
    Dieser code fügt die 3-Zustände Funktionalität an den oberkategorie-check-alle checkboxen hinzu
    und updatet id-unterkategorie-... checkboxen welche durch an bzw abwahl des oberkategorie-check-alle geändert werden müssen.

    Des Weiteren wird bei einem update einer der id-unterkategorie-... checkboxen deren zugehörige oberkategorie-check-alle checkbox geupdatet.

 */
/* Action listener Funktion für die Oberkategorien */
function ok_checkall_handler(e, uk_checkbox_list) {
    let state = e.target.checked;
    /* alle unterkategorien-checkboxen werden den Zustand der oberkategorie-checkbox gesetzt */
    for (let i = 0; i < uk_checkbox_list.length; i++) {
        uk_checkbox_list[i].checked = state;
    }
    update_mv();
}

/* Action listener Funktion für die Unterkategorien */
function uk_check_handler(e, ok_checkall, uk_checkbox_list) {
    let all_true = true;
    let all_false = false;
    /* alle unterkategorien-checkboxen werden auf ihren Zustand überprüft */
    for (let i = 0; i < uk_checkbox_list.length; i++) {
        all_true = all_true && uk_checkbox_list[i].checked;
        all_false = all_false || uk_checkbox_list[i].checked;
    }
    all_false = !all_false;

    /* Oberkategorie-checkbox wird passend der unterkategorien gesetzt */
    if (all_true) {
        ok_checkall.checked = true;
        ok_checkall.indeterminate = false;
    } else if (all_false) {
        ok_checkall.checked = false;
        ok_checkall.indeterminate = false;
    } else {
        ok_checkall.checked = false;
        ok_checkall.indeterminate = true;
    }
    update_mv();
}

/* unterkategorien und oberkategorien werden den action listenern zugewiesen*/
for (let i = 0; i < ok_button_list.length; i++) {
    /*  für jede oberkategorie-checkbox werden die zugehörigen unterkategorien-checkboxen geholt*/
    let ok_checkall = ok_button_list[i].getElementsByClassName("oberkategorie-check-alle")[0];
    let related_uk_checkbox_list = ok_button_list[i].nextElementSibling.querySelectorAll('input[type="checkbox"]');

    ok_checkall.addEventListener("click", function (e) {
        ok_checkall_handler(e, related_uk_checkbox_list);
    });
    for (let j = 0; j < related_uk_checkbox_list.length; j++) {
        related_uk_checkbox_list[j].addEventListener("click", function (e) {
            uk_check_handler(e, ok_checkall, related_uk_checkbox_list);
        });
    }
}


/*
    Dieser Code behandelt die Funktionalität der sidebar bei kleineren Bildschirm groessen
    
 */
const header = document.getElementById("kopf");
const massnahme_list = document.getElementById("id-massnahmen-liste");
const sidebar = document.getElementById("sidebar");
const sidebar_button = document.getElementById("sidebar-oeffner");
const sidebar_mediaquerry = window.matchMedia("(min-width: 1000px)");

function sidebar_handler(e) {
    if (e instanceof MouseEvent) {
        if (e.target === sidebar_button || e.target.parentNode === sidebar_button) {
            sidebar.classList.add("sidebar-aktiv");
            massnahme_list.setAttribute("style", "pointer-events:none;");
            header.setAttribute("style", "pointer-events:none;");
        } else if (!sidebar.contains(e.target)) {
            sidebar.classList.remove("sidebar-aktiv");
            massnahme_list.removeAttribute("style");
            header.removeAttribute("style");
        }
    } else if (e instanceof MediaQueryListEvent) {
        if (e.matches) {    // Das Gerät ist breiter als 1000px!
            sidebar.classList.remove("sidebar-aktiv");
        }
    }
}

sidebar_mediaquerry.addEventListener("change", function (e) {
    sidebar_handler(e)
});
document.body.addEventListener("click", function (e) {
    sidebar_handler(e)
});

const informatione = document.getElementsByClassName("info-button");
window.addEventListener('resize', function (e) {
    for (let i = 0; i < informatione.length; i++) {
        informatione[i].blur();
    }
});