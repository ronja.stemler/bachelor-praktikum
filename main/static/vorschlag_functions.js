var search = document.getElementById("search");
const uk_list = document.querySelectorAll('[id^="id-unterkategorie-"]');
const priorisierungen_liste = document.getElementsByClassName("priorisierungen");

/*
    Die hilfsfunktion sorgt dafür, dass die eingabe in die Search debounced wird,
    dh nicht nach jedem eingegebenen Buchstaben wird eine Request getätigt
 */
function util_debounce(func, wait, immediate) {
    let timeout;
    return function () {
        let context = this, args = arguments;
        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

/*
    Die hilfsfunktion sorgt dafür, dass eine Tabgroup sich mit deb Pfeiltasten und
    der Tabulatortaste steuern lässt. Dies wird ausschließlich für die search-vorschläge verwendet
 */
function util_manage_tabgroups() {
    let tabgroup_list = document.querySelectorAll("[data-tabgroup]");
    for (let i = 0; i < tabgroup_list.length; i++) {
        let tabgroup_entries = tabgroup_list[i].querySelectorAll("[tabindex]");
        for (let j = 0; j < tabgroup_entries.length; j++) {
            tabgroup_entries[j].addEventListener("keydown", function (index, tabgroup_entries, e) {
                // Bei Tab oder Pfeiltaste-Runter geht der Focus einen Eintrag niedriger bei Pfeiltaste-Hoch hoch. 
                if (e.key === "Tab" || e.key === "ArrowDown") {
                    e.preventDefault();
                    if (tabgroup_entries[index + 1]) {
                        tabgroup_entries[index + 1].focus();
                    } else {
                        tabgroup_entries[0].focus();
                    }
                } else if (e.key === "ArrowUp") {
                    e.preventDefault();
                    if (tabgroup_entries[index - 1]) {
                        tabgroup_entries[index - 1].focus();
                    } else {
                        tabgroup_entries[tabgroup_entries.length - 1].focus();
                    }
                }
            }.bind(null, j, tabgroup_entries))
        }
    }
}

/*
 *  Diese Funktion updated die Massnahmenliste Asynchron. Dazu wird ein Query-String gebaut, welcher mit einer XMLHttpRequest
 *  gesendet wird. Die Antwort wird in 2 teile zerlegt: 1. Die aus der Suche resultierenden Massnahmen 2. Die aus dem Suchbegriff   
 *  resultierenden Suchvorschläge
 */
function update_mv() {
    let req = new XMLHttpRequest();
    // Diese Funktion behandelt die Antwort der Query
    req.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const parser = new DOMParser();
            const htmlDoc = parser.parseFromString(req.response, 'text/html');
            let sv = "";
            try {
                sv = htmlDoc.getElementById("SV__REPLACE").innerHTML
                htmlDoc.getElementById("SV__REPLACE").outerHTML = "";
            } catch (error) {
            }
            document.getElementById("id-massnahmen-liste").innerHTML = htmlDoc.body.innerHTML;
            document.getElementById("suchvorschlaege").innerHTML = sv.trim();
            util_manage_tabgroups();
        }
    }
    // Dieser Abschnitt behandelt die Erstellung des Query-Strings und das Senden der Request
    let str_uk = '';
    let str_pr = '';
    // Werte aller AUSGEWÄHLTEN Unterkategorien werden in den Query-String aufgenommen
    for (let i = 0; i < uk_list.length; i++)
        if (uk_list[i].checked)
            str_uk += "&" + uk_list[i].name + "=" + uk_list[i].value;
    // Werte aller AKTIVEN Priorisierungen werden in den Query-String aufgenommen
    for (let i = 0; i < priorisierungen_liste.length; i++) {
        let prio = priorisierungen_liste[i].querySelectorAll("input");
        let prio_checkbox = prio[0];
        let prio_slider = prio[1];
        if (prio_checkbox.checked)
            str_pr += "&" + prio_slider.name + "=" + prio_slider.value;
    }
    // Searchstring wird in den Query-String aufgenommen
    let query_string = search.value !== "" ? "search=" + search.value : "";
    query_string += str_uk + str_pr;
    query_string = query_string !== "" ? "/?" + query_string : "/";


    req.open("GET", query_string, true);
    req.setRequestHeader("MX-Request", "ture");
    req.send();

}

const debounced_update_mv = util_debounce(update_mv, 250);
search.addEventListener("input", debounced_update_mv);