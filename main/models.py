from django.db import models


# Model für die Kategorien mit Attributen
class Kategorie(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Kategorien"

    def __str__(self):
        return self.name


# Model für die Unterkategorien mit den Attributen Name, Fremdschlüssel und Platz für ein Icon
class UnterKategorie(models.Model):
    name = models.CharField(max_length=255)
    ueber_kategorie = models.ForeignKey(Kategorie, on_delete=models.CASCADE)
    icon = models.FileField(upload_to='icons/', blank=True)

    class Meta:
        verbose_name_plural = "Unterkategorien"

    def __str__(self):
        return self.name


# Model für eine Massnahme mit Attributen
class Massnahme(models.Model):
    # Maßnahme Beschreibung und Quelle müssen großgeschrieben werden wegen import
    Maßnahme = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    Beschreibung = models.TextField(blank=True)
    Quelle = models.TextField(blank=True)
    unter_kategorien = models.ManyToManyField(UnterKategorie, blank=True)

    class Meta:
        verbose_name_plural = "Maßnahmen"

    def __str__(self):
        return self.Maßnahme


# Modelliert die Priorisierungen Kriterien und deren strings für einzelne Punktzahlen
class Priorisierungen(models.Model):
    name = models.CharField(max_length=255)
    name_0_punkte = models.CharField(max_length=255)
    name_1_punkte = models.CharField(max_length=255)
    name_2_punkte = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = "Priorisierungskategorien"

    def __str__(self):
        return self.name


# Modelliert die m:n Verbindung zwischen Priorisierungen und Massnahmen mittels einer Zahl (Wertung)
class Prioriserungs_Wertung(models.Model):
    priorisierung = models.ForeignKey(Priorisierungen, on_delete=models.CASCADE)
    massnahme = models.ForeignKey(Massnahme, on_delete=models.CASCADE)
    wertung = models.IntegerField(default=0)

    class Meta:
        verbose_name_plural = "Priorisierungswertungen"


# Modell für die
class Infotexte(models.Model):
    css_id = models.CharField(max_length=255)
    informationen = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = "Infotexte"
