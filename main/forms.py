import os.path

from django import forms
from django.conf import settings
from django.contrib.admin.helpers import ActionForm
from django.utils.translation import gettext_lazy as _
from import_export.forms import ImportExportFormBase
from django.views.generic.edit import FormView


#Input Form für die Imports mit Weiterleitung definieren.
#Aus Definitionen von Forms zusammengeführt.
class VollerImportForm(ImportExportFormBase):    
    import_file = forms.FileField(
        label=_('File to import')
    )
    input_format = forms.ChoiceField(
        label=_('Format'),
        choices=(),
    )

    def __init__(self, import_formats, *args, **kwargs):
        super().__init__(*args, **kwargs)
        choices = [
            (str(i), f().get_title())
            for i, f in enumerate(import_formats)
        ]
        if len(import_formats) > 1:
            choices.insert(0, ('', '---'))
            self.fields['import_file'].widget.attrs['class'] = 'guess_format'
            self.fields['input_format'].widget.attrs['class'] = 'guess_format'

        self.fields['input_format'].choices = choices

    @property
    def media(self):
        extra = "" if settings.DEBUG else ".min"
        return forms.Media(
            js=(
                f'admin/js/vendor/jquery/jquery{extra}.js',
                'admin/js/jquery.init.js',
                'import_export/guess_format.js',
            )
        )


#Input Forms für Upload der Icons        
class IconUploadForm(forms.Form):
    icons = forms.FileField(
        widget=forms.ClearableFileInput(attrs={'multiple':True}),
        label=_('Hochzuladende Icons')
    )
    
#Definiere Klasse für die Darstellung des Icon Upload Forms
class FileFieldFormView(FormView):
    form_class = IconUploadForm

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        files = request.FILES.getlist('file_field')
        if form.is_valid():
            return self.form_valid(form)

#Input Forms für Upload des Logos
class LogoUploadForm(forms.Form):
    logo = forms.FileField(
        label=_('Logo aussuchen')
    )
