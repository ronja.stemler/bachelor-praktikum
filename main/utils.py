from .models import *
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

import nltk
import spacy

spacy.cli.download("de_core_news_sm")
nltk.download('punkt')
nlp = spacy.load("de_core_news_sm")

"""
:param massnahmen: Liste von Massnahmen 
:returns: Liste von Liste von Priorisierungskriterien und dazugehörigen Werten. In Reihenfolge von massnahmen_liste
"""


def get_prio_zu_massnahme(massnahmen_liste):
    min_val, max_val = 0, 3

    # Dictionary für alle Priorisierungen mit: Numerischer Wert -> String
    alle_prio_key = list(Priorisierungen.objects.all().values_list('name'))
    alle_prio_value = list(Priorisierungen.objects.all().values_list('name_0_punkte', 'name_1_punkte', 'name_2_punkte'))
    alle_prio = {alle_prio_key[i][0]: alle_prio_value[i] for i in range(len(alle_prio_key))}

    prioliste = []
    # über jede massnahme wird iteriert
    for massnahme in massnahmen_liste:
        key_liste = []
        wert_liste = []
        massnahme_prio_list = dict(
            Prioriserungs_Wertung.objects.filter(massnahme=massnahme).values_list('priorisierung__name', 'wertung'))
        # Für jede Priorisierung wird der zugehörige Wert der Massnahme,
        # in einen für die Priorisieung passenden string umgewandelt.
        for key, value in zip(massnahme_prio_list.keys(), massnahme_prio_list.values()):
            if max_val > value >= min_val:
                string = alle_prio[key][value]
            else:
                string = 'wrong_value'
            key_liste.append(key)
            wert_liste.append(string)

        prioliste.append(zip(key_liste, wert_liste))
    return prioliste


"""
:param massnahmen: Liste von Massnahmen 
:returns: Liste von Liste von Liste von Unterkategorien,
            die  gruppiert nach Oberkategorien in Reihenfolge der Massnahmen angegeben sind.
            (m1:(ok1:(uk1),(uk8)),(ok2:(uk4),(uk6),(uk9)),..),(m2:(ok1:(uk3),..)..)..
"""


def get_ok_mit_icons(massnahmen_liste, ober_kategorien_liste):
    uk_sorted = []
    for massnahme in massnahmen_liste:
        # Für Massnahme liste aller unterkategorien aussortiert, welche icons haben
        uk_mit_icons = [unterkategorie for unterkategorie in
                        UnterKategorie.objects.filter(massnahme__Maßnahme=massnahme.Maßnahme) if
                        unterkategorie.icon.__bool__()]

        icons_pro_massnahme = []
        for kategorie in ober_kategorien_liste:
            icons_pro_ok = [uk for uk in uk_mit_icons if uk.ueber_kategorie == kategorie]
            if len(icons_pro_ok) != 0:
                icons_pro_massnahme.append(icons_pro_ok)
        uk_sorted.append(icons_pro_massnahme)
    return uk_sorted


"""
:param massnahmen: Liste von Massnahmen 
:returns: Liste von Quellen, die zu den Massnahmen gehören
"""


def get_quellen_zu_massnahmen(massnahmen_liste):
    seperator = '\n'
    val = URLValidator()
    quellen_zu_massnahmen = []

    # Für jede Massnahme wird das Quellen-Feld der Massnahmen in eine Liste von Quellen aufgeteilt
    for massnahme in massnahmen_liste:
        quellen_liste = str(massnahme.Quelle).split(sep=seperator)
        ist_link_liste = []
        # Jede Quelle der quellen_liste einer Massnahme, wird überprüft ob sie ein Link ist
        for quelle in quellen_liste:
            try:
                val(quelle)
                ist_link_liste.append(True)
            except ValidationError:
                ist_link_liste.append(False)
        # quellen_liste und ist_link_liste werden zu einem iterierbaren Objekt
        # und werden an quellen_zu_massnahmen angehangen
        quellen_zu_massnahmen.append(zip(quellen_liste, ist_link_liste))

    return quellen_zu_massnahmen


"""
:param massnahmen: Liste von Namen von Unterkategorien zb.  ['Material', 'Druckluft']
:returns: Set von Massnahmen welche zu den unterkategorien passen
"""


def get_massnahmen_zu_unterkategorien(unter_kategorien):
    massnahmen = Massnahme.objects.all()
    alle_kategorien = set(Kategorie.objects.all())

    setlist = []
    for k in alle_kategorien:
        uk_list = UnterKategorie.objects.filter(ueber_kategorie=k, id__in=unter_kategorien)
        if uk_list:
            setlist.append(Massnahme.objects.filter(unter_kategorien__in=uk_list))

    if setlist:
        for queryset in setlist:
            massnahmen = massnahmen.intersection(queryset, massnahmen)
    massnahmen = massnahmen.order_by('Maßnahme')

    return massnahmen


"""
:param slider: 2D-Liste von Slider-Namen und deren Wertung 
:param massnahmen: Liste von Massnahmen 
:returns: Liste von Massnahmen welche nach ihrer Punktzahl abhängig der Slider sortiert sind
"""


def sort_massnahmen_nach_prio(slider, massnahmen_liste):
    m_list = []
    if len(slider) == 0:
        sorted(massnahmen_liste, key=lambda x: x.Maßnahme)
        return massnahmen_liste

    for m in massnahmen_liste:
        # Für jede Massnahme wird ein Dictionary erstellt welches jeweils den Namen und Wert einer Priorisierung enthält
        m_prio_list = dict(
            Prioriserungs_Wertung.objects.filter(massnahme=m).values_list('priorisierung__name', 'wertung'))

        # Für jeden Slider in der Slider-Liste wird dessen Gewichtung mit dem Wert der für die Massnahme vergebene
        # Priorisierung verrechnet und auf punkte addiert
        punkte = 0

        for slider_name, slider_wert in slider:
            m_prio_wert = m_prio_list.get(slider_name)
            # Falls die aktuelle Massnahme m einen Wert, für den die Priorisierung slider_name hat, wird dieser mit dem
            # Wert slider_wert des sliders Multipliziert und auf die gesamtpunktzahl addiert
            if m_prio_wert is not None:
                punkte = punkte + m_prio_wert * int(slider_wert)
        # Jede Massnahme wird mit ihrer Punktebewertung an eine 2-Tupel Liste aller Massnahmen Angehangen
        m_list.append((punkte, m))

    # Die 2-Tupel-Liste aller Massnahmen und ihren Punktebewertungen wird nach Punkten sortiert
    m_list.sort(key=lambda x: x[0], reverse=True)

    # Die ursprüngliche Massnahmen-Liste wird durch die Massnahmen in Reihenfolge der 2-Tupel-Liste ersetzt
    massnahmen_liste = [m_l[1] for m_l in m_list]

    return massnahmen_liste


"""
:param suchbegriff: Suchbegriff als String
:param massnahmen_liste: Liste von Massnahmen 
:returns: Liste von 5 Vorschlägen zur vervollständigung Suchbegriffes, 
            Optionaler Vorschlag als Ersatz des Suchbegriffes 
"""


def generate_vorschlaege(suchbegriff, massnahmen_liste):
    # in Maßnahmen wird ähnliches Wort gesucht
    max_vorschlaege = 5

    similar = []
    dist = []
    for v in massnahmen_liste:
        tokens = nltk.word_tokenize(v.Maßnahme) + nltk.word_tokenize(v.Beschreibung)
        tokens = [i for i in tokens if len(i) > 3]
        for token in tokens:

            if nltk.edit_distance(suchbegriff.lower(), token.lower()) > 0:
                if len(suchbegriff) / nltk.edit_distance(suchbegriff.lower(), token.lower()) > 3:
                    similar.append(token)

            if suchbegriff.lower() == token.lower()[0:len(suchbegriff)]:
                spa = nlp(token)
                for t in spa:
                    if t.pos_ == "NOUN" or t.pos_ == "VERB" or t.pos_ == "PROPN":
                        dist.append(token)

    vorschlag = []
    rechtschreibfehler = ""
    if len(dist) == 0 and similar:
        rechtschreibfehler = similar[0]

    elif len(dist) > 0:
        dist = nltk.FreqDist(dist)
        if suchbegriff != "":
            for i in dist.most_common(max_vorschlaege):
                if len(dist.most_common(max_vorschlaege)) > 0:
                    vorschlag.append(i[0])

    if suchbegriff in vorschlag:
        vorschlag.remove(suchbegriff)
    if suchbegriff.capitalize() in vorschlag:
        vorschlag.remove(suchbegriff.capitalize())

    return vorschlag, rechtschreibfehler


"""
:param css_ids: string mit css_ids der infotexte
:returns: dict mit : css_ids -> Texte der i's
"""


def get_infotexte(css_ids):
    d = {}
    for i in css_ids:
        try:
            d[i] = Infotexte.objects.get(css_id=i).informationen
        except:
            d[i] = ""
    return d
