from import_export import resources

#Erweiterte Klasse für Ressourcen Import um transponieren zu ermöglichen
class transposableResource(resources.ModelResource):
    #Override für Funktion um Transponieren zu ermöglichen
    #Unterschied in Zeile 23
    def import_data_inner(
            self, dataset, dry_run, raise_errors, using_transactions,
            collect_failed_rows, rollback_on_validation_errors=False, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.total_rows = len(dataset)
        db_connection = self.get_db_connection_name()

        if using_transactions:
            # when transactions are used we want to create/update/delete object
            # as transaction will be rolled back if dry_run is set
            sp1 = resources.savepoint(using=db_connection)

        try:
            with resources.atomic_if_using_transaction(using_transactions, using=db_connection):
                #Rückgabewert ermöglicht
                dataset=self.before_import(dataset, using_transactions, dry_run, **kwargs)
        except Exception as e:
            resources.logger.debug(e, exc_info=e)
            tb_info = resources.traceback.format_exc()
            result.append_base_error(self.get_error_result_class()(e, tb_info))
            if raise_errors:
                raise

        instance_loader = self._meta.instance_loader_class(self, dataset)

        # Update the total in case the dataset was altered by before_import()
        result.total_rows = len(dataset)

        if collect_failed_rows:
            result.add_dataset_headers(dataset.headers)

        for i, row in enumerate(dataset.dict, 1):
            with resources.atomic_if_using_transaction(using_transactions, using=db_connection):
                row_result = self.import_row(
                    row,
                    instance_loader,
                    using_transactions=using_transactions,
                    dry_run=dry_run,
                    row_number=i,
                    raise_errors=raise_errors,
                    **kwargs
                )
            result.increment_row_result_total(row_result)

            if row_result.errors:
                if collect_failed_rows:
                    result.append_failed_row(row, row_result.errors[0])
                if raise_errors:
                    raise row_result.errors[-1].error
            elif row_result.validation_error:
                result.append_invalid_row(i, row, row_result.validation_error)
                if collect_failed_rows:
                    result.append_failed_row(row, row_result.validation_error)
                if raise_errors:
                    raise row_result.validation_error
            if (row_result.import_type != resources.RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.append_row_result(row_result)

        if self._meta.use_bulk:
            # bulk persist any instances which are still pending
            with resources.atomic_if_using_transaction(using_transactions, using=db_connection):
                self.bulk_create(using_transactions, dry_run, raise_errors)
                self.bulk_update(using_transactions, dry_run, raise_errors)
                self.bulk_delete(using_transactions, dry_run, raise_errors)

        try:
            with resources.atomic_if_using_transaction(using_transactions, using=db_connection):
                self.after_import(dataset, result, using_transactions, dry_run, **kwargs)
        except Exception as e:
            resources.logger.debug(e, exc_info=e)
            tb_info = resources.traceback.format_exc()
            result.append_base_error(self.get_error_result_class()(e, tb_info))
            if raise_errors:
                raise

        if using_transactions:
            if dry_run or \
                    result.has_errors() or \
                    (rollback_on_validation_errors and result.has_validation_errors()):
                resources.savepoint_rollback(sp1, using=db_connection)
            else:
                resources.savepoint_commit(sp1, using=db_connection)

        return result