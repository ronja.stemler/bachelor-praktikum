from django.urls import path
from django.contrib import admin
from django.contrib.auth import views as auth_views

from . import views

# urls der Website
urlpatterns = [
# url für die Hauptseite für den Nutzer
path("",views.home, name="home"),
# urls für den Admin
path('admin/login/', auth_views.LoginView.as_view(template_name='admin_login.html'), name='login'), #new
path('admin/', admin.site.urls),
]