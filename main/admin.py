import tablib
from django.contrib import admin
from django.urls import path, reverse
from django.http import HttpResponse, HttpResponseRedirect
from import_export import tmp_storages
from import_export import resources
from import_export.admin import ImportMixin
from .resources import transposableResource
from .models import *
from .forms import VollerImportForm
from .views import icon_upload,logo_upload

#Funktion um für die spaltenbasierten Importe irrelevante Zeilen zu entfernen (nach Transponierung)
def ausschussEntfernen(dataset, string):
    ausschuss=dataset.lpop()
    while ausschuss[0]!='Anfang ' + string:
        ausschuss=dataset.lpop()
    ausschuss=dataset.rpop()
    while ausschuss[0]!='Ende ' + string:
        ausschuss=dataset.rpop()


#Zu importierende Struktur definieren und importmethoden Anpassung
class Kategorien_importieren(transposableResource):
    class Meta:
        model = Kategorie
        #transactionsmodus für undo
        use_transactions = True
        skip_unchanged = True
        #bestimmung des key Feldes
        import_id_fields = ('name',)
        #Liste der gewünschten importe
        fields = ('id','name',)
        
    #Manipulation der Tabelle vor dem Import um die Kategorien in den Zeilen statt der Spalten zu haben
    def before_import(self, dataset, dry_run, *args, **kwargs):
        dataset = dataset.transpose()
        ausschussEntfernen(dataset, 'Kategorien')
        einträge = dataset.dict
        erste_spalte=dataset.headers[0]
        #neue Tabelle befüllen (um die Zeilen auf die die Kategorien gestreckt werden zu entfernen)
        dataset = tablib.Dataset(headers=['name', 'delete'], title=dataset.title)
        for item in einträge:
                if(item[erste_spalte]!=None):
                    row = [item[erste_spalte],0]
                    dataset.append(row)
        #Gelöschte oder umbenannte Kategorien für Löschung eintragen
        Liste=dataset.__getitem__('name')
        for object in Kategorie.objects.all():
            if object.name not in Liste:
                row = [object.name, 1]
                dataset.append(row)
        return dataset
    
    #Definiere Bedingung zum Löschen
    def for_delete(self, row, instance):
        return (row['delete'])    

#Zu importierende Struktur definieren und importmethoden Anpassung            
class UnterKategorien_importieren(transposableResource):
    class Meta:
        model = UnterKategorie
        #transactionsmodus für undo
        use_transactions=True
        skip_unchanged = True
        #bestimmung des key Feldes
        import_id_fields = ('name',)
        #Liste der gewünschten importe
        fields = ('id','name','ueber_kategorie','icon')
    
    #Manipulation der Tabelle vor dem Import um die Unterkategorien in den Zeilen statt der Spalten zu haben
    def before_import(self, dataset, dry_run, *args, **kwargs):
        dataset=dataset.transpose()
        headers = dataset._get_headers()
        #ändere Nr. zu name
        headers[0] = 'kategorie'
        headers[2] = 'name'
        dataset._set_headers(headers)
        ausschussEntfernen(dataset, 'Kategorien')
        #neue Tabelle befüllen (um die Kategorien zuweisen zu können)
        einträge = dataset.dict
        dataset = tablib.Dataset(headers=['name', 'kategorie', 'delete'], title=dataset.title)
        momentane_über_kategorie=None
        for item in einträge:
            if(item['name']!=None):
                if(item['kategorie']!=None):
                    momentane_über_kategorie=item['kategorie']
                else:
                    item['kategorie']=momentane_über_kategorie
                row = [item['name'], item['kategorie'], 0]
                dataset.append(row=row)
        #Gelöschte oder umbenannte Unterkategorien für Löschung eintragen
        Liste=dataset.__getitem__('name')
        for object in UnterKategorie.objects.all():
            if object.name not in Liste:
                row = [object.name, object.ueber_kategorie, 1]
                dataset.append(row)
        return dataset
    
    #Tausche Kategorie Namen gegen id  und füge icon ein
    def before_import_row(self, row, **kwargs):
        if(Kategorie.objects.filter(name=row['kategorie']).exists()):
            row['ueber_kategorie']=Kategorie.objects.filter(name=row['kategorie']).get().id
        row['icon'] = 'icons/' + row['name'].replace('/','') + '.svg'
    
    #Löschkriterium bestimmen
    def for_delete(self, row, instance):
        return (row['delete'])
        
#Zu importierende Struktur definieren und importmethoden Anpassung        
class Priorisierungskriterien_importieren(transposableResource):
    class Meta:
        model = Priorisierungen
        #transactionsmodus für undo
        use_transactions=True
        skip_unchanged = True
        #bestimmung des key Feldes
        import_id_fields = ('name',)
        #Liste der gewünschten importe
        fields = ('id','name','name_0_punkte','name_1_punkte','name_2_punkte',)
        
    #Manipulation der Tabelle vor dem Import um die Priorisierungskriterien in den Zeilen statt der Spalten zu haben
    def before_import(self, dataset, dry_run, *args, **kwargs):
        dataset=dataset.transpose()
        headers = dataset._get_headers()
        #ändere Nr. zu name
        headers[0] = 'name'
        headers[1] = 'delete'
        headers[2] = 'stufen'
        dataset._set_headers(headers)
        ausschussEntfernen(dataset, 'Priorisierungskriterien')
        #Gelöschte oder umbenannte Priorisierungskriterien für Löschung eintragen
        Liste=dataset.__getitem__('name')
        for object in Priorisierungen.objects.all():
            if object.name not in Liste:
                row = [None for key in dataset.headers]
                row[0] = object.name
                row[1] = 'delete'
                row[2] = '//'
                dataset.append(row)
        return dataset

    #Teile Stufen in Einzelteile und füge ein
    def before_import_row(self, row, **kwargs):
        stufen = row['stufen'].split("/")
        row['name_0_punkte'] = stufen[0]
        row['name_1_punkte'] = stufen[1]
        row['name_2_punkte'] = stufen[2]
    
    #Löschungskriterium definieren    
    def for_delete(self, row, instance):
        return (row['delete']=='delete')

class Massnahmen_importieren(resources.ModelResource):
    class Meta:
        model = Massnahme
        #transactionsmodus für undo
        use_transactions=True
        skip_unchanged = True
        #bestimmung des key Feldes
        import_id_fields = ('Maßnahme',)
        #Liste der gewünschten importe
        fields = ('id','Maßnahme','Beschreibung','Quelle','unter_kategorien',)
        
    #Manipulation der Tabelle vor dem Import   
    def before_import(self, dataset, dry_run, *args, **kwargs):
        #entferne Reihen 2 und 3 aus import-Datensatz, da keine Maßnahmen
        dataset.lpop()
        row3 = dataset.lpop()
        row1 = dataset._get_headers()
        #übernehme Unterkategorie-Bezeichnungen in header um Zugang zu Kategorienamen zu haben
        name_index = None
        for i, h in enumerate(row3):
            if row1[i] == 'Maßnahme':
                name_index = i 
            if row3[i]!=None: 
                row1[i] = row3[i]
        dataset._set_headers(row1)
        #Gelöschte oder umbenannte Maßnahmen für Löschung eintragen
        Liste=dataset.__getitem__('Maßnahme')
        for object in Massnahme.objects.all():
            if (object.Maßnahme not in Liste):
                row = [None for key in dataset.headers]
                row[name_index]=object.Maßnahme
                row[name_index+1]='delete'
                dataset.append(row)
    
    
    def before_import_row(self, row, **kwargs):
        #ändere null für Quellen und Beschreibungen zu leeren Strings für not Null constraint
        if (row['Quelle']==None): row['Quelle']=''
        if (row['Beschreibung']==None): row['Beschreibung']=''
        row['unter_kategorien']=''
        #konkateniere unterkategorie id in m2m
        for kategorie, haken in row.items():
            if (kategorie != None):
                if (UnterKategorie.objects.filter(name=kategorie).exists() and haken!=None):
                    kat_id = UnterKategorie.objects.filter(name=kategorie).get().id
                    row['unter_kategorien']+=str(kat_id) + ',' 
    
    #Löschkriterium bestimmen
    def for_delete(self, row, instance):
        return (row['Anfang Kategorien']=='delete')

#Zu importierende Struktur definieren und importmethoden Anpassung
class Priorisierungswertungen_importieren(transposableResource):
    class Meta:
        model = Prioriserungs_Wertung
        #transactionsmodus für undo
        use_transactions=True
        skip_unchanged = True
        #bestimmung der key Felder
        import_id_fields = ('massnahme','priorisierung',)
        #Liste der gewünschten importe
        fields = ('id','massnahme','priorisierung','wertung',)
        #überschreiben der Daten-auslese-Funktion um erweiterte Modifikation des Datensets durch before_import zu ermöglichen(Einträge ver-x-fachen)
        #einzige relevante änderung ist self.before_import -> dataset=self.before_import
    
    #Manipulation der Tabelle vor dem Import um pro Maßnahmen-Priorisierungs Kombination eine Zeile zu haben
    def before_import(self, dataset, dry_run, *args, **kwargs):
        #entferne Reihen 2 und 3 aus import-Datensatz, da keine Maßnahmen
        dataset.lpop()
        dataset.lpop()
        #kopiere alle Einträge und erstelle neues, leeres Datenset um Schreibschutz zu umgehen
        einträge = dataset.dict
        dataset = tablib.Dataset(headers=('massnahme','priorisierung','wertung'), title=dataset.title)
        #Iteriere nach Priorisierungskriterien über die Einträge um Einen Eintrag pro Kombination von Maßnahme und Kriterium zu erstellen und in die Liste einzufügen
        for item in einträge:
            for object in Priorisierungen.objects.all():
                if(item['Maßnahme']!=None):
                    dataset.append([Massnahme.objects.filter(Maßnahme=item['Maßnahme']).get().id,object.id,item[object.name]])
        return dataset
        
        
#Admin Anpassung, damit Import-Knopf erscheint        
class BasisAdmin(ImportMixin, admin.ModelAdmin):
    tmp_storage_class = tmp_storages.CacheStorage
    import_form_class = VollerImportForm

    #Weiterleitungen des Bestätigen und weiter Buttons definieren
    def process_result(self, result, request):
        HttpResponseUrl = super().process_result(result, request)
        if request.POST.get('confirmgo'):
            currentModel = self.get_model_info()[1]
            if(currentModel == 'kategorie'):
                url = reverse('admin:%s_unterkategorie_import' % self.get_model_info()[0],current_app=self.admin_site.name)
                return HttpResponseRedirect(url)
            elif(currentModel == 'unterkategorie'):
                url = reverse('admin:%s_priorisierungen_import' % self.get_model_info()[0],current_app=self.admin_site.name)
                return HttpResponseRedirect(url)
            elif(currentModel == 'priorisierungen'):
                url = reverse('admin:%s_massnahme_import' % self.get_model_info()[0],current_app=self.admin_site.name)
                return HttpResponseRedirect(url)
            elif(currentModel == 'massnahme'):
                url = reverse('admin:%s_prioriserungs_wertung_import' % self.get_model_info()[0],current_app=self.admin_site.name)
                return HttpResponseRedirect(url)
        return HttpResponseUrl

#Admin Anpassung, damit Import-Knopf erscheint
class KategorienAdmin(BasisAdmin):
    resource_classes = [Kategorien_importieren,]

#Admin Anpassung, damit Import-Knopf erscheint
class UnterKategorienAdmin(BasisAdmin):
    resource_classes = [UnterKategorien_importieren,]

    #Uploads der Icons und des Logos eine URL geben
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('icon_upload/', self.admin_site.admin_view(icon_upload)),
            path('logo_upload/', self.admin_site.admin_view(logo_upload))
        ]
        return my_urls + urls

#Admin Anpassung, damit Import-Knopf erscheint
class PriorisierungenAdmin(BasisAdmin):
    resource_classes = [Priorisierungskriterien_importieren,]

#Admin Anpassung, damit Import-Knopf erscheint
class MassnahmenAdmin(BasisAdmin):
    resource_classes = [Massnahmen_importieren,]

#Admin Anpassung, damit Import-Knopf erscheint
class PrioriserungsWertungAdmin(BasisAdmin):
    resource_classes = [Priorisierungswertungen_importieren,]

admin.site.register(Massnahme, MassnahmenAdmin)
admin.site.register(Kategorie, KategorienAdmin)
admin.site.register(UnterKategorie, UnterKategorienAdmin)
admin.site.register(Priorisierungen, PriorisierungenAdmin)
admin.site.register(Prioriserungs_Wertung, PrioriserungsWertungAdmin)
admin.site.register(Infotexte, MassnahmenAdmin)