FROM debian:latest
EXPOSE 80

ARG COLOR1='\033[0;35m'
ARG COLOR2='\033[0;32m'
ARG NC='\033[0m'
ARG NAME=Massnahmenkatalog
ARG CONTAINER_NAME="${NAME}_container"

# copy files
COPY  bp_2022 app/bp_2022
COPY  main app/main
COPY  media app/media
COPY  nginx app/nginx

COPY entrypoint.sh app/
COPY requirements.txt app/
COPY manage.py app/
COPY db.sqlite3 app/
WORKDIR /app
RUN ls -la

RUN printf "[${COLOR1}${NAME}${NC}]${COLOR2} copied files \n${NC}"

# Install dependencies
RUN apt-get update
RUN apt-get -y install nginx libaugeas0 gettext-base python3 python3-venv python-is-python3

# create venv
RUN python -m venv /opt/venv && \
    /opt/venv/bin/python -m pip install -q pip --upgrade && \
    /opt/venv/bin/python -m pip install -qqq -r requirements.txt
RUN printf "[${COLOR1}${NAME}${NC}]${COLOR2} created venv \n${NC}"

RUN /opt/venv/bin/python -m pip install certbot certbot-nginx
RUN printf "[${COLOR1}${NAME}${NC}]${COLOR2} Installed dependencies \n${NC}"

# un/set necessary links
RUN unlink /etc/nginx/sites-enabled/default
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log
RUN ln -s /opt/venv/bin/certbot /usr/bin/certbot
RUN printf "[${COLOR1}${NAME}${NC}]${COLOR2} link nginx logs to container stdout \n${NC}"

# ,ake migrations and collect static files
RUN /opt/venv/bin/python manage.py collectstatic --noinput --verbosity 0
RUN rm -rf main/migrations/
RUN /opt/venv/bin/python manage.py makemigrations main --noinput --verbosity 0

ENV DJANGO_DEBUG=0
COPY . $DJANGO_DEBUG


RUN chmod +x entrypoint.sh
CMD ["./entrypoint.sh"]