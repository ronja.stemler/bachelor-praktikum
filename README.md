# PTW Massnahmenkatalog

Um in der Industrie Energie einzusparen, gibt es eine Vielzahl an möglichen Maßnahmen, bspw. kann man alte
Aggregate austauschen, leerlaufende elektrische Förderbänder durch Schwerkraftförderbänder ersetzen oder
Maschinen abschalten, wenn sie nicht genutzt werden.
Für Unternehmen stellt es allerdings häufig eine große Herausforderung dar, die besten Maßnahmen für ihre
Produktion zu identifizieren.
Der PTW Maßnahmenkatalog ist ein interaktives Tool, mit dessen Hilfe Anwender*innen unterstützt werden, die besten
Energieeffizienzmaßnahmen für ihre Produktion zu finden.
___

## Build

Zuvor zu installierende Pakete:

- wsl2 (nur Windows)
- docker
- git

Repository klonen (eins von beiden):

    git clone git@git.rwth-aachen.de:ronja.stemler/bachelor-praktikum.git
    git clone https://git.rwth-aachen.de/ronja.stemler/bachelor-praktikum.git

Docker Image erstellen:

    cd bachelor-praktikum
    docker build -q -t <beliebiger_name> -f Dockerfile .

### Docker Image als Datei speichern (Optional):

#### Linux & MacOS

    docker save <beliebiger_name> | gzip > <beliebiger_name>.tar.gz

___

## Nutzung

### Docker Image aus Datei laden (Optional):

    docker load < <beliebiger_name>.tar.gz

### Docker Container aus Image erstellen und ausführen (mit oder ohne Domain):

    docker run --rm -p 80:80 --name <beliebiger_container_name> <beliebiger_name>
    docker run --rm -p 80:80 --name <beliebiger_container_name> --env DOMAIN=example.com <beliebiger_name>
#### Individueller Port
    docker run --rm -p 1234:80 --name <beliebiger_container_name> --env PORT=1234 <beliebiger_name> #Container akzeptiert Anfragen von http://127.0.0.1:1234 
    docker run --rm -p 1234:80 --name <beliebiger_container_name> --env PORT=1234 --env DOMAIN=example.com <beliebiger_name> # Container akzeptiert Anfragen von http://example.com:1234


_Achtung, beim ersten Start des Containers wird ein Superuser generiert und_ __einmalig__ _angezeigt (Beispiel):_

    Achtung diese Nachricht wird nur einmal angezeigt : 
    Nutzername: admin_9PY5
    Passwort: Opov-xmwTATFj0Lk

### Login und Daten-Upload
Siehe Abgabedokumentation
### Richtlinien für die PTW Energie-/Ressourcensparmaßnahmen Exceltabelle

- Relevante Spalten müssen die relevante Bezeichnung in der ersten Zeile haben oder etwas in der ersten Zeile und die
  relevante Bezeichnung in der dritten Zeile
- Maßnahmen müssen eindeutig benannt sein.
    - Keine leeren Maßnahmenbezeichnungen
    - Keine duplizierten Maßnahmenbezeichnungen
- Unterkategorien müssen eindeutig benannt sein.
    - Keine leeren Unterkategoriebezeichnungen
    - Keine duplizierten Unterkategoriebezeichnungen
- Unterkategorie-Zellen unterscheiden zwischen etwas oder nichts enthalten.
- Priorisierungskriterien-Zellen müssen natürliche Zahlen oder nichts beinhalten.
- Leer- und Sonderzeichen sind in Texten und Namen erlaubt
    - Leerzeichen am Ende eines Zelleninhaltes sind nicht irrelevant
- Beschreibungs- und Quell-Zellen dürfen leer sein

___
## Abhängigkeiten
- Django 			Version: 4.1+
- Django-Import-Export 	Version: 3.0.1
- Pillow 			Version: 9.3.0+
- Gunicorn		Version: 20.1.0+
- spaCy			Version: 3.5.0
- NumPy			Version: 1.23.4
- NLTK			Version: 3.8.1
___
## Autoren

- Arzbach, Markus
- Ott, Maurits
- Spitzer, Maike
- Stemler, Ronja
- Zimmermann, Henrike