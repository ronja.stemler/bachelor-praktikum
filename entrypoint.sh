#!/bin/bash
set -e

RUN_PORT="8000"

### NGINX CONFIG ###

# CHECK IF SERVER SHOULD RUN ON ANY IP
# CHECK IF SERVER HAS RUN BEFORE
if [ ! -f ".setup_done" ]; then
  # CHECK IF SERVER HAS A DOMAIN
  if [[ -z "${DOMAIN}" ]]; then
    mv nginx/nginx_intranet.conf.tpl /etc/nginx/sites-available/local
    ln -s /etc/nginx/sites-available/local /etc/nginx/sites-enabled/local
  else
    export proxy_add_x_forwarded_for="\$proxy_add_x_forwarded_for"
    export host="\$host"
    envsubst <nginx/nginx_internet.conf.tpl >$DOMAIN
    unset proxy_add_x_forwarded_for
    unset host
    mv $DOMAIN /etc/nginx/sites-available/$DOMAIN
    ln -s /etc/nginx/sites-available/$DOMAIN /etc/nginx/sites-enabled/$DOMAIN
  fi
fi

### DJANGO CONFIG ###
# SET DJANGO DOMAIN
if [ -z ${DOMAIN+x} ]; then
  export DJANGO_HOSTS="localhost,127.0.0.1"
else
  export DJANGO_HOSTS=$DOMAIN
fi

# SET DJANGO DEBUG
# export DJANGO_DEBUG=0

# SET DJANGO KEY
if [ ! -f "django_secret.key" ]; then
  /opt/venv/bin/python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())' >django_secret.key
fi
VALUE=$(cat django_secret.key)
export DJANGO_KEY=$VALUE
# GENERATE DEFAULT USER
if [ ! -f ".setup_done" ]; then
  export DJANGO_SUPERUSER_USERNAME="admin_"$(
    tr </dev/urandom -dc A-Z-a-z-0-9 | head -c${1:-4}
    echo
  )
  export DJANGO_SUPERUSER_PASSWORD=$(
    tr </dev/urandom -dc A-Z-a-z-0-9 | head -c${1:-16}
    echo
  )
  export DJANGO_SUPERUSER_EMAIL="admin@example.com"
  printf '\033[0;35m'
  printf "Achtung diese Nachricht wird nur einmal angezeigt : "
  printf '\033[0m'
  printf "\nNutzername: "$DJANGO_SUPERUSER_USERNAME"\nPasswort: "$DJANGO_SUPERUSER_PASSWORD"\n"
  /opt/venv/bin/python manage.py createsuperuser --noinput
fi

### RUN SERVER ###
touch .setup_done
/opt/venv/bin/gunicorn bp_2022.wsgi:application --bind "0.0.0.0:${RUN_PORT}" --daemon
nginx -g 'daemon off;'
